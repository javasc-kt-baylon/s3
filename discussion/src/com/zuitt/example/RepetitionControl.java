package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    // [SECTION] Loops
    // are control structures that allow code blocks to be executed multiple times

    public static void main(String[] args) {
        // While loop
        // allow for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate through is indefinite.
//        int x = 0;
//        while(x < 10){
//            System.out.println("Loop number: " + x);
//            x++;
//        }

        // Do-while loop
        // similar to while loops. However, do-while loops will always execute at least once - while loops may not execute at all.
//        int y = 10;
//        do{
//            System.out.println("Countdown: " + y);
//            y--;
//        } while (y > 10);

        // For loop
        // Syntax
        /*
         * for(initialVal; condition; iteration){
         *   // code block
         * }
         */
        for(int i = 0; i < 10; i++){
            //int i = 0 -> initial value
            //i < 10 -> limiting expression/condition
            //i++ -> increment/decrement
            System.out.println("Current count: " + i);
        }

        //For loop with array
        int[] intArray = {100, 200, 300, 400, 500};
        for (int i = 0; i < intArray.length; i++){
                System.out.println(intArray[i]);

        }

        //For-each loop with array
        /*
        * Syntax:
        *   for(dataType itemName : arrayName) {
        *       //codeblock
        * }
        *
        *
        * */

        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for (String name: nameArray) {
            System.out.println(name);
        }

        // Nested loops with multidimensional arrays
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //outerloop
        for(int row = 0; row < 3; row++) {
            //inner loop
            for (int col = 0; col < 3; col++) {
                System.out.println("classroom["+ row +"][" + col + "] = " + classroom[row][col]);
            }
        }

        //for-each loop with multidimensional array
        //if someone ask a question
        //accessing each row
        for(String[] row : classroom){
            //accessing each column (actual element)
            for (String column : row) {
                System.out.println(column);
            }
        }

        //for-each with ArrayList
        // Syntax:
        /*
         *   arrayListName.foreach(Consumer<E> ->// code block);
         *   "->" This is called lambda operator which is used to separater parameter and implementation
         */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("ArrayList: " + numbers);

        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        //for-each with HashMaps
        // Syntax:
        /*
         *   hashMapNames.foreach((key, value)-> // code block);
         *       "->" This is called lambda operator which is used to separater parameter and implementation
         */
        HashMap<String, Integer> grades = new HashMap<>(){{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        grades.forEach((subject, grade) -> System.out.print(subject + " : " + grade + "\n"));

    }
}
